//
//  Promise+Operators.swift
//  Promises
//
//  Created by Nikolay Petrov on 9/5/15.
//  Copyright © 2015 Atlassian. All rights reserved.
//

import Foundation

precedencegroup PromiseRecoverPrecedence {
    associativity: left
    higherThan: AssignmentPrecedence
}

precedencegroup PromiseThenPrecedence {
    associativity: left
    higherThan: PromiseRecoverPrecedence
}

infix operator |> : PromiseThenPrecedence
/**
    The operator variant of `Promise.then`, when the block is returning a value
*/
@discardableResult
public func |> <T, U>(left: Promise<T>, right: @escaping (T) throws -> U) -> Promise<U> {
    return left.then {
        try right($0)
    }
}

infix operator |~> : PromiseThenPrecedence
/**
    The operator variant of `Promise.then`, when the block is returning a promise
*/
@discardableResult
public func |~> <T, U>(left: Promise<T>, right: @escaping (T) throws -> Promise<U>) -> Promise<U> {
    return left.then {
        try right($0)
    }
}

infix operator !> : PromiseRecoverPrecedence
/**
    The operator variant of `Promise.recover`, when the block is returning a value
*/
@discardableResult
public func !> <T>(left: Promise<T>, right: @escaping (Error) throws -> Void) -> Promise<T> {
    return left.recover {
        try right($0)
    }
}

infix operator &> : PromiseThenPrecedence
/**
    The operator variant of `Promise.thenInBackground`, when the block is returning a value
*/
@discardableResult
public func &> <T, U>(left: Promise<T>, right: @escaping (T) throws -> U) -> Promise<U> {
    return left.thenInBackground {
        try right($0)
    }
}

infix operator &~> : PromiseThenPrecedence
/**
    The operator variant of `Promise.thenInBackground`, when the block is returning a promise
*/
@discardableResult
public func &~> <T, U>(left: Promise<T>, right: @escaping (T) throws -> Promise<U>) -> Promise<U> {
    return left.thenInBackground {
        try right($0)
    }
}

infix operator !&> : PromiseRecoverPrecedence
/**
 The operator variant of `Promise.recoverInBackground`, when the block is returning a value
 */
@discardableResult
public func !&> <T>(left: Promise<T>, right: @escaping (Error) throws -> Void) -> Promise<T> {
    return left.recoverInBackground {
        try right($0)
    }
}

prefix operator |<
public prefix func |< <T>(value: T) -> Promise<T> {
    return Promise<T>.make(with: value)
}
public prefix func |< <T>(gen: () -> T) -> Promise<T> {
    return Promise<T>.make(with: gen())
}
